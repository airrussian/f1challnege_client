unit uUpdataCenter;

interface

	uses System.SysUtils, System.Classes, uDebug, uNetFile, uFiles, Variants, Vcl.StdCtrls;

	Type	
		TBuilders = array of TDownloadThread;
		
	var
		LastError:String;
		Status:String;

	procedure Init(const sNetBuilder, sLocBuilder, sAppName:String);

	function unpackBuilders():Boolean;		

	function GetListBuilders(var Builders:TBuilders):Boolean;
	

implementation

	uses Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc;

	Var
		NetBuilder, LocBuilder, AppName:String;

	procedure Init(const sNetBuilder, sLocBuilder, sAppName:String);
	begin
		NetBuilder:=sNetBuilder;
		LocBuilder:=sLocBuilder;
	end;

	function unpackBuilders():Boolean;
	var
		unpack:TUnPack;	
		orderbuilder: array[1..10] of String = ('root', 'audio', 'options', 'season', 'tracks', 'driver', 'vehicles', 'user', 'other', 'other2');		
		i:1..10;
	begin
		for i := 1 to 10 do
		begin			
			if (FileExists(LocBuilder + orderbuilder[I])) then
			begin
				unpack:=TUnPack.Create(false); 
				unpack.SetParam(orderbuilder[I], '90210');
				unpack.WaitFor; 
				FreeAndNil(unpack);
			end;		
		end;
		// Path: update/root.ud -> � ������! (����� ��������� ������ � ��� ����� � ����)
		// Path: update/audio.ud -> /AUDIO ( ����� ������������� OPTIONS )
		// Path: update/options.ud -> /OPTIONS - �������, /AUDIO/OPTIONS/ - ������� )
		// Path: update/season.ud -> /SeasonData
		// Path: update/tracks.ud -> /SeasonData/Circuits
		// Path: update/driver.ud -> /SeasonData/Drivers ( ���� ����� ������ ������ )
		// Path: update/vehicles.ud -> /SeasonData/Vehicles ( ��� ���� ���������!! )
		// Path: update/user.ud -> /Save (��� �������������� ����� ������������ ����� � INI)		
	end;

	function GetListBuilders(var Builders:TBuilders):Boolean;
	var
		XML:TXMLDocument;
		Root:IXMLNode;
		Count, I, J:Word;
		hash:String;
		FileName:String;
		ListBiulders:TDownloadThread;		
		F:TextFile;
		Str:String;
		Size:Cardinal;
	begin	
		ListBiulders:=TDownloadThread.Create(false); 
		ListBiulders.SetParam(AppName, NetBuilder + 'index.php', LocBuilder + 'list');
		ListBiulders.WaitFor; 
		FreeAndNil(ListBiulders);

		AssignFile(F, LocBuilder + 'list');
		Reset(F);
		while Not(EOF(F)) do
		begin
			Readln(F, Str);
			Hash:=Copy(Str,1,32);
			Size:=StrToInt(Copy(Str, 33, Pos('-', Str, 33) - 33));
			FileName:=Copy(Str, Pos('-', Str, 33)+1, Length(Str) - Pos('-', Str, 33));
			if (not uFiles.CheckHash(LocBuilder + FileName, Hash)) then
			begin
				Inc(J);
				SetLength(Builders, J);
				Builders[J-1]:=TDownloadThread.Create(false);
				Builders[J-1].SetParam(AppName, NetBuilder + FileName, LocBuilder + FileName);
				Builders[J-1].Priority:=tpLower;		
				Builders[J-1].Resume;	
			end;
		end;		
		CloseFile(F);		
		Result:=True;	
	end;

{	function check():boolean;
	var
		RCode:Byte;
	begin
		if (uNetFile.Download('f1client.exe', 'http://94.73.211.6/sr/update/test.gz', 'test.gz')) then
		begin
			Debug('checksumma = '+ IntToStr(FileCRC32('test.gz')));

		end;
	end;

	// ������� �������� ������ ����������!
	function checkUpDataFiles():Boolean;
	begin

	end;}

end.
