unit _dataChallenge;

interface

  uses Winapi.Windows, System.SysUtils;

	Type
		TVector = array[1..3] of Double;

		TDataChallenge = class
    private
    public
			function timenow():Extended;
			procedure control();

			function GetByte(address:Cardinal):Byte;
			function GetWord(address:Cardinal):Word;
			function GetSingle(address:Cardinal):Single;
			function GetDouble(address:Cardinal):Double;
		end;

implementation

{ TDataChallenge }

procedure TDataChallenge.control;
begin
	
end;

function TDataChallenge.GetByte(address: Cardinal): Byte;
begin

end;

function TDataChallenge.GetDouble(address: Cardinal): Double;
begin

end;

function TDataChallenge.GetSingle(address: Cardinal): Single;
begin

end;

function TDataChallenge.GetWord(address: Cardinal): Word;
begin

end;

function TDataChallenge.timenow: Extended;
begin
  result:=now;
end;



end.
