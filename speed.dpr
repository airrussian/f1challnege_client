library speed;

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  DateUtils,
  D3DX8,
  Direct3D8,
  draw in 'draw.pas',
  f1c_data in 'f1c_data.pas',
  _control in '_control.pas';

{$R picture.RES}

Type
  TPresent = function (self: IDirect3DDevice8; pSourceRect, pDestRect: PRect; hDestWindowOverride: HWND; pDirtyRegion: PRgnData): HResult; stdcall;
  TEndScene = function (self: IDirect3DDevice8): HResult; stdcall;

  TArray5b = array[0..4] of byte;

Var
  Present:TPresent;
  EndScene:TEndScene;
  Save, Jump: TArray5b;

  saveMS:Cardinal = 0;
  fps:Double;

procedure Drawing(self:IDirect3DDevice8);
begin
  draw.init(self);
  if (f1c_data.ontrack()) then
  begin
    f1c_data.calc;
    draw.speedmetr(f1c_data.GetFloat($005F0B18));
    draw.gear(f1c_data.GetShortInt($007C9318));
    draw.Text(0, 400, FloatToStr(f1c_data.GetKERS));
    draw.Text(0, 440, IntToStr(GetTickCount));
    draw.kers(f1c_data.GetKERS);
    draw.drs(GetStatusDRS());
    draw.throttle(f1c_data.throttle);
    draw.break(f1c_data.break);
    draw.RPM(f1c_data.GetDouble($007C908C));
  end;
end;

function HookEndScene(self:IDirect3DDevice8):HResult; Stdcall;
begin
  CopyMemory(@EndScene, @save, 5);                                               // ���������� ����������� ���������� ������� Present
  Drawing(self);                                                                   // ������ ���-�� ���
  EndScene(self);     // �������� ������������ ������� Present
  CopyMemory(@EndScene, @jump, 5);                                               // ����� ������������� JUMP �� ���� (���������� ����)
end;

function HookPresent(self:IDirect3DDevice8; pSourceRect, pDestRect: PRect; hDestWindowOverride: HWND; pDirtyRegion: PRgnData):HResult; Stdcall;
begin
  CopyMemory(@present, @save, 5);                                               // ���������� ����������� ���������� ������� Present
  Drawing(self);                                                                   // ������ ���-�� ���
  present(self, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);     // �������� ������������ ������� Present
  CopyMemory(@present, @jump, 5);                                               // ����� ������������� JUMP �� ���� (���������� ����)
end;

function getJump(addr1, addr2:Cardinal):TArray5b;
type
  TArray4b = array[1..4] of byte;
var
  a4b:TArray4b;
  i:byte;
begin
  a4b:=TArray4b(addr1 - addr2 - 5);
  result[0]:=$E9; for i := 1 to 4 do result[i]:=a4b[i];
end;

var
  temp:Cardinal;

Procedure HookDX;
var
  addr:Cardinal;
  offset:Cardinal;
  F:File of byte;
begin
  addr:=Cardinal(GetModuleHandle('D3D8.DLL'));
  AssignFile(F, 'offset'); Reset(F); BlockRead(F, offset, 4); CloseFile(F);

  EndScene:=TEndScene(Pointer(Cardinal(addr+offset)));
  jump:=getJump(Cardinal(@HookEndScene), Cardinal(@EndScene));                   // ���������� JUMP ������� ���� ���� ������� �� Present, ��� �� ��������� � HookPresent
  CopyMemory(@save, @EndScene, 5);                                               // ���������� 5-�� ������������ ����������
  VirtualProtect(@EndScene, 5, PAGE_EXECUTE_READWRITE, temp);                    // ������������� ����� �� ����������� ���������� ������� �������� ������
  CopyMemory(@EndScene, @jump, 5);                                               // ������������� JUMP �� ������� HookPresent}
end;

begin
  HookDX;
  CreateThread(nil, 0, @_control.init, nil, 0, temp);                           // ����� ����������
//  CreateThread(nil, 0, @Data, nil, 0, temp);                                  // ����� ����������/��������� ������ ���� � ����
end.
