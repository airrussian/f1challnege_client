object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 462
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Position = poScreenCenter
  OnClick = FormClick
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Update: TGauge
    Left = 32
    Top = 432
    Width = 473
    Height = 22
    Progress = 0
  end
  object Label1: TLabel
    Left = 32
    Top = 285
    Width = 182
    Height = 13
    Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1074' '#1087#1088#1086#1094#1077#1089#1089#1077' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1080
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Title: TLabel
    Left = 40
    Top = 16
    Width = 449
    Height = 116
    Caption = 'sunrace-f1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -96
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label0: TLabel
    Left = 32
    Top = 266
    Width = 182
    Height = 13
    Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1074' '#1087#1088#1086#1094#1077#1089#1089#1077' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1080
    Color = clBackground
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object PopupMenu1: TPopupMenu
    TrackButton = tbLeftButton
    Left = 488
    Top = 344
    object pm_rungame: TMenuItem
      Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1080#1075#1088#1091
      OnClick = pm_rungameClick
    end
    object pm_settings: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object pm_exit: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      OnClick = pm_exitClick
    end
  end
  object TrayIcon1: TTrayIcon
    PopupMenu = PopupMenu1
    Left = 488
    Top = 296
  end
  object CheckUpdate: TTimer
    Interval = 10000
    OnTimer = CheckUpdateTimer
    Left = 248
    Top = 288
  end
  object ProgressUpdate: TTimer
    Enabled = False
    OnTimer = ProgressUpdateTimer
    Left = 320
    Top = 288
  end
end
