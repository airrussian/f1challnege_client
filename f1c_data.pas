unit f1c_data;

interface

  uses Windows;

  const
    DRS_CLOSE = 0;
    DRS_ENABLED = 1;
    DRS_OPEN = 2;
    DRS_DISABLED = 3;


  function GetDouble(address:Cardinal):Double;
  function GetByte(address:Cardinal):Byte;
  function GetShortInt(address:Cardinal):ShortInt;
  function GetFloat(address:Cardinal):Single;

  function SetFloat(address:Cardinal; S:Single):Boolean;
  function SetDouble(address:Cardinal; D:Double):Boolean;

  function ontrack:boolean;

  procedure SetStatusDRS(Status:Byte);
  function GetStatusDRS():Byte;

  function throttle():Single;
  function break():Single;

  procedure KERS;
  function GetKERS:Double;

  procedure calc;

implementation

  var
    SaveDRS:Single = 0;
    StatusDRS:Byte = DRS_CLOSE;

    KersSec:Double = 7;
    SaveMS:Cardinal = 2000000000;

  procedure calc;
  var
    P:Pointer;
  begin
    if GetAsyncKeyState(VK_F2) <> 0 then SetStatusDRS(DRS_OPEN);
    if GetAsyncKeyState(VK_F3) <> 0 then KERS;

    if (GetByte($007C747F) > 0) then StatusDRS:=DRS_CLOSE;
    case StatusDRS of
      DRS_OPEN: SetFloat($007C8EE8, SaveDRS * 0.5);
      DRS_CLOSE, DRS_DISABLED: SetFloat($007C8EE8, SaveDRS);
    end;
  end;

//  function GetBrikes
  function GetStatusDRS():Byte;
  begin
    Result:=StatusDRS;
  end;

  procedure SetStatusDRS(Status:Byte);
  begin
    if (Status=DRS_OPEN) AND (StatusDRS<>DRS_DISABLED) then StatusDRS:=Status;
  end;

  function ontrack:boolean;
  begin
    if GetByte($007C9081)=1 then
    begin
      if (SaveDRS = 0) then SaveDRS:=GetFloat($007C8EE8);
      result:=true;
    end
    else
    begin
      SaveDRS:=0;
      result:=false;
    end;
  end;

  function GetDouble(address:Cardinal):Double;
  var
    D:PDouble;
  begin
    D:=Pointer(address);
    Result:=D^;
  end;

  function GetFloat(address:Cardinal):Single;
  var
    S:PSingle;
  begin
    S:=Pointer(address);
    Result:=S^;
  end;

  function GetByte(address:Cardinal):Byte;
  var
    B:PByte;
  begin
    B:=Pointer(address);
    Result:=B^;
  end;

  function GetShortInt(address:Cardinal):ShortInt;
  var
    S:PShortInt;
  begin
    S:=Pointer(address);
    Result:=S^;
  end;

  function SetFloat(address:Cardinal; S:Single):Boolean;
  var
    F:PSingle;
  begin
    F:=Pointer(address);
    F^:=S;
  end;

  function SetDouble(address:Cardinal; D:Double):Boolean;
  var
    F:PDouble;
  begin
    F:=Pointer(address);
    F^:=D;
  end;

  function throttle():Single;
  var
    b:byte;
  begin
    b:=GetByte($007C7476);
    result:=(b - 140)/100;
    if ( result < 0) then result:=0;
    if ( result > 1) then result:=1;
  end;

  function break():Single;
  var
    b:byte;
  begin
    b:=GetByte($007C747E);
    result:=(b - 140)/100;
    if ( result < 0) then result:=0;
    if ( result > 1) then result:=1;
  end;

  procedure KERS;
  var
    T:Longint;
    RPM:Double;
  begin
    RPM:=GetDouble($007C908C);
    SetDouble($007C9084, RPM + 100);

    T:=GetTickCount();
    KersSec:=KersSec - (T - saveMS) / 1000;
    if (KersSec < 0) then KersSec:=0;
    SaveMS:=T;
  end;

  function GetKERS:Double;
  begin
    result:=KersSec;
  end;

end.
