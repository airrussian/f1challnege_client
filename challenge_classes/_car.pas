unit _car;

interface

	uses _dataChallenge, _drs, _energy, _kers, _wheel;

	Type
		TCar = class(TDataChallenge)
		private
			position:TVector;
			speed:Single;	
			Energy:TEnergy;
			Wheel_lf:TWheel;
			Wheel_rf:TWheel;
			Wheel_lr:TWheel;
			Wheel_rr:TWheel;
			Kers:TKers;
			DRS:TDrs;
		public	
			constructor create;			
		end;

implementation

{ TCar }

constructor TCar.create;
begin
	speed:=0;
	Energy:=TEnergy.Create;
	
	Wheel_lf:=TWheel.Create();
	Wheel_rf:=TWheel.Create;
	Wheel_lr:=TWheel.Create;
	Wheel_rr:=TWheel.Create;

	Kers:=TKers.Create();
	DRS:=TDRS.Create;
end;

end.
