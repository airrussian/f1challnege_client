unit uMain;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.ExtCtrls,
	Vcl.Samples.Gauges, Vcl.StdCtrls, Vcl.Buttons, IdBaseComponent,
  IdAntiFreezeBase, Vcl.IdAntiFreeze;

type
	TForm1 = class(TForm)
		PopupMenu1: TPopupMenu;
		TrayIcon1: TTrayIcon;
		pm_exit: TMenuItem;
		N2: TMenuItem;
		pm_rungame: TMenuItem;
		pm_settings: TMenuItem;
		Update: TGauge;
		Label1: TLabel;
    Title: TLabel;
		CheckUpdate: TTimer;
    Label0: TLabel;
    ProgressUpdate: TTimer;
		procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure pm_exitClick(Sender: TObject);
    procedure pm_rungameClick(Sender: TObject);
    procedure CheckUpdateTimer(Sender: TObject);
		procedure FormShow(Sender: TObject);
    procedure ProgressUpdateTimer(Sender: TObject);
  private
    { Private declarations }
    procedure TestDX8;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses jpeg, inifiles, D3DX8, Direct3D8, uUpdataCenter, uDebug, uSecurityApp, uNetFile, uFiles;

var
  processId:Cardinal;

  jpg:TJpegImage;
  cfg:TIniFile;

function OSVersion: string;
var
  VerInfo: TOSVersionInfo;
begin
  fillchar(VerInfo, sizeof(VerInfo), 0);
  VerInfo.dwOSVersionInfoSize := SizeOf(VerInfo);
	result := '';
  if GetVersionEx(VerInfo) then
	if VerInfo.dwPlatformId = VER_PLATFORM_WIN32_NT then
	result := IntToStr(VerInfo.dwMajorVersion) + '.' + IntToStr(VerInfo.dwMinorVersion);
end;

procedure TForm1.TestDX8;
Type
  TDirect3DCreate8 = function (SDKVersion: LongWord): Pointer; Stdcall;
	TPresent = function (self: IDirect3DDevice8; pSourceRect, pDestRect: PRect; hDestWindowOverride: HWND; pDirtyRegion: PRgnData): HResult; stdcall;
  TArrayPointer = array[1..40] of Cardinal;
var
  hcDLL:THandle;
  Direct3DCreate8: TDirect3DCreate8;
  D3D:IDirect3D8;
  Device8:IDirect3DDevice8;

  wnd:THandle;

  d3dpp:_D3DPRESENT_PARAMETERS_;
  d3ddm:_D3DDISPLAYMODE;

  address:Cardinal;
  p:Pointer;

  tblMethod:TArrayPointer;

  present:TPresent;

  i:Byte;

  f:File of byte; offset:Cardinal;
begin
  hcDLL:=LoadLibrary('D3D8');
  if hcDLL <= HINSTANCE_ERROR then
  begin
    Debug('�� �������� ��������� D3D8.DLL');
    MessageDlg('�� ���� ��������� D3D8.DLL', mtError, [mbOk], 0);
    Exit;
  end else
    Debug('hcDLL = ' + IntToHex(Cardinal(hcDLL), 2));
  offset:=Cardinal(hcDLL);

  // ������� ����� Direct3DCreate8
  Direct3DCreate8 := GetProcAddress(hcDLL, 'Direct3DCreate8');
  if not Assigned(Direct3DCreate8) then
  Begin
    Debug('Direct3DCreate8 �� ������� ����� �����');
    MessageDlg('Direct3DCreate8 �� ������� ����� �����', mtError, [mbOk], 0);
    FreeLibrary(hcDLL);
    Exit;
  End else
    Debug('myDirect3DCreate8 = ' + IntToHex(Cardinal(@Direct3DCreate8), 2));

  // ������� ��������� IDirect3D8
  D3D:=IDirect3D8(Direct3DCreate8(D3D_SDK_VERSION));
  if (D3D = nil) then
  begin
    Debug('������ �������� ���������� IDirect3D8');
    MessageDlg('������ �������� ���������� IDirect3D8', mtError, [mbOk], 0);
    FreeLibrary(hcDLL);
    exit;
  end else
    Debug('D3D = ' + IntToHex(Cardinal(D3D), 2));

  // ������� ����� ��� �������� �������
  if FAILED(D3D.GetAdapterDisplayMode(D3DADAPTER_DEFAULT, d3ddm)) then
  begin
    Debug('������ AdapterDisplayMod');
    MessageDlg('������ AdapterDisplayMode', mtError, [mbOk], 0);
    D3D:=nil;
    FreeLibrary(hcDLL);
    Exit;
  end else
    debug('GetAdapterDisplayMode - �������');

  ZeroMemory(@d3dpp, sizeof(d3dpp));
  With d3dpp Do
  begin
    Windowed:=BOOL(TRUE);
    SwapEffect:=D3DSWAPEFFECT_DISCARD;
    BackBufferWidth := 800;
    BackBufferHeight := 600;
    BackBufferFormat:=d3ddm.Format;
  end;

  // ������� ������������� ����!
  wnd:=CreateWindowA('STATIC','test',0,0,0,800,600,0,0,0,nil);
  if (wnd = 0) then
  begin
    Debug('������ �������� �������������� ����');
    MessageDlg('������', mtError, [mbOk], 0);
    D3D:=nil;
    FreeLibrary(hcDLL);
    Exit;
  end;

  // ������� ���������� ��� ������ �������!
  if (FAILED(D3D.CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, wnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, d3dpp, Device8))) Then
  begin
    debug('�� ������� ������� CreateDevice');
    showmessage('�� ������� ������� CreateDevice');
    exit;
  end else begin
    debug('Device8 = ' + IntToHex(Cardinal(Device8), 2));
  end;

  //Pointer(Cardinal())^

  address:=Cardinal(Pointer(Cardinal(Device8))^);
  debug('Device8 pointer tablemethod = '+IntToHex(address, 2));
  p:=Pointer(address);

  tblMethod := TArrayPointer(p^);  // ������� ���������� �� ������ D3DDev
  for i := 1 to 40 do debug('method['+inttostr(i)+'] = '+IntToHex(tblMethod[i], 2));

  offset:=tblMethod[36] - offset;

  AssignFile(F, 'offset'); Rewrite(F); BlockWrite(F, offset, 4); CloseFile(F);

  Device8:=nil;
  D3D:=nil;

  FreeLibrary(hcDLL);
  CloseHandle(wnd);
end;

var
	Builders:TBuilders;

procedure TForm1.CheckUpdateTimer(Sender: TObject);
var
	I:Word;
begin	
	CheckUpdate.Enabled:=False;
	if (GetListBuilders(Builders)) and (Length(Builders) > 0) Then 
		ProgressUpdate.Enabled:=True				
	else 
		CheckUpdate.Enabled:=True;
end;

procedure TForm1.ProgressUpdateTimer(Sender: TObject);
var
	I:Integer;
	Complete:Boolean;
begin
	Complete:=True;
	for I := 0 to Length(Builders)-1 do	
	begin
		if not Builders[I].Finished then
		begin
			Builders[I].Terminate;
			TLabel(Form1.FindComponent('Label'+IntToStr(I))).Caption:=' Thread ' + IntToStr(Builders[I].ThreadID) + ' is work, loading = ' + IntToStr(Builders[I].GetProgress)
		end
		else
			TLabel(Form1.FindComponent('Label'+IntToStr(I))).Caption:=' Thread ' + IntToStr(Builders[I].ThreadID) + ' is complete ';
		Complete:=Complete AND Builders[I].Finished;
	end;
	if (Complete) then
	begin
		SetLength(Builders, 0);
		ProgressUpdate.Enabled:=False;
		CheckUpdate.Enabled:=True;		
	end;
end;


procedure TForm1.FormClick(Sender: TObject);
begin
 //Form1.Hide;
 // Trayicon1.Visible:= True;
 Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
	bg,t:String;
	v1,v2:Integer;
begin                                                                                                                  
	cfg:=TIniFile.Create(ExtractFileDir(Application.ExeName) + '\cfg.ini');
	
	uUpdataCenter.Init(
			cfg.ReadString('BILDERS', 'NET', 'http://onrace.ru/f1c/sunrace/2014SR/release/'), 
			cfg.ReadString('BILDERS', 'LOC', ExtractFilePath(Application.ExeName) + 'builders\'),
			ExtractFileName(Application.ExeName)			
	);
			
	//jpg:=TJpegImage.Create;
	//jpg.LoadFromFile(bg);

	if (cfg.ReadBool('DEBUG', 'Enabled', true)) then uDebug.init(cfg.ReadString('DEBUG', 'FileName', GetCurrentDir() + '/debug.log'));

	if (not uSecurityApp.Security) then begin Debug('��������� ����� ������'); ShowMessage('������? ������� � airrussian@mail.ru � �������� �������, � �������� ������'); halt; end;
	//if (not IsUserAnAdmin) then begin Debug('������ �� �� ����� ������'); ShowMessage('��������� ���������� �� ����� ��������������'); Halt; end;

  debug('Function FormCreate');
	debug('Windows = ' + OSVersion);

	
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  uDebug.save;
  cfg.Free; cfg:=nil;
  jpg.Free; jpg:=nil;
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  Canvas.Draw(0,0, jpg);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
	CheckUpdateTimer(self);
end;

procedure TForm1.pm_exitClick(Sender: TObject);
begin
  Close;
end;


procedure TForm1.pm_rungameClick(Sender: TObject);
var
  DLLPath:PWideChar;
  wnd:THandle;
  ProcessInfo:TProcessInformation;
  StartUpInfo:TStartUpInfo;
  hProc, hThread, hKernel: THandle;
  BytesToWrite, BytesWritten: SIZE_T;
  pRemoteBuffer, pLoadLibrary: Pointer;
  dwThreadID: Cardinal;
begin
  TestDX8;

  DLLPath:='speed.dll';

  FillChar(StartUpInfo, SizeOf(TStartUpInfo), 0);
  With StartUpInfo do
  begin
    cb := SizeOf(TStartUpInfo);
    dwFlags := STARTF_USESHOWWINDOW or STARTF_FORCEONFEEDBACK;
    wShowWindow := SW_SHOWNORMAL;
  end;

  if (CreateProcess('run.exe', '', nil, nil, true, NORMAL_PRIORITY_CLASS, nil, nil, StartUpInfo, ProcessInfo)) then
  begin
      WaitForInputIdle(ProcessInfo.hProcess, INFINITE); // ���� ���������� �������������

      BytesToWrite := SizeOf(WideChar)*(Length(DLLPath) + 1);
      pRemoteBuffer := VirtualAllocEx(ProcessInfo.hProcess, nil, BytesToWrite, MEM_COMMIT, PAGE_READWRITE);
      if pRemoteBuffer = nil then begin showmessage('error'); exit; end;
      try
        if not WriteProcessMemory(processInfo.hProcess, pRemoteBuffer, DLLPath, BytesToWrite, BytesWritten) then  begin showmessage('error'); exit; end;
        hKernel := GetModuleHandle('kernel32.dll');
        pLoadLibrary := GetProcAddress(hKernel, 'LoadLibraryW');
        hThread := CreateRemoteThread(processInfo.hProcess, nil, 0, pLoadLibrary, pRemoteBuffer, 0, dwThreadId);
        try
          WaitForSingleObject(hThread, INFINITE);
        finally
          CloseHandle(hThread);
        end;
      finally
        VirtualFreeEx(processInfo.hProcess, pRemoteBuffer, 0, MEM_RELEASE);
      end;

      CloseHandle(ProcessInfo.hThread);                 // ��������� ���������� ������
      CloseHandle(ProcessInfo.hProcess);                // ��������� ���������� ��������
  end;

end;


end.
