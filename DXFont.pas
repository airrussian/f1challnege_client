unit DXFont;

interface

  uses
    Direct3D8, D3DX8, System.SysUtils, Winapi.Windows;

  var
    DX_FONT_VERDANA_10_BOLD:ID3DXFont;
    DX_FONT_TNR_36_NORMAL:ID3DXFont;

  Procedure CreateFonts(device: IDirect3DDevice8);

implementation

  Function Verdana10Bold:HFont;
  var
    LogFont:TLogFont;
  begin
    FillChar(LogFont, SizeOf(LogFont), 0);
    With LogFont do
    Begin
      lfHeight                := 10;                // ������ ������
      lfWidth                 := 10;                // ������ ������
      lfEscapement            := 0;                 // ����, � ������� �������, ����� �������� ������� � ���� � ����������. ������ ����� ����������� ������� ����� ������ ������.
      lfOrientation           := 0;                 // ����, � ������� ����� �������, ����� ������� ����� ������� ��������� � ��� � ����������.
      lfWeight                := FW_BLACK;          // ��� ������ � ��������� �� 0 �� 1000. ��������, 400 �������� ���������� � 700 ����������. ���� ��� �������� ����� ����, ��� ���������.
      lfItalic                := 0;                 // ��������� �����, ���� ����������� �������� TRUE .
      lfUnderline             := 0;                 // ������������ ������, ���� ����������� �������� TRUE .
      lfStrikeOut             := 0;                 // ������������ ������, ���� ����������� �������� TRUE .
      lfCharSet               := RUSSIAN_CHARSET;   // ����� ��������. ��������� �������� ��������������.
      lfOutPrecision          := OUT_DEFAULT_PRECIS;// ����� ��������. �������� ������ ����������, ��������� ������ ����� ������ ��������������� ������ �������������� ������, ������ ���������� ��������, �����, ����, � ��� ������. ��� ����� ���� ���� �� ��������� ��������.
      lfClipPrecision         := CLIP_DEFAULT_PRECIS;  // �������� ���������. �������� ��������� ����������, ��� �������� �������, ������� �������� ��� ������� ���������. ��� ����� ���� ���� ��� ��������� �� ��������� ��������.
      lfQuality               := ANTIALIASED_QUALITY;  // �������� ������.
      lfPitchAndFamily        := DEFAULT_PITCH;  // ������ � ��������� ������.
      StrCopy(lfFaceName, 'Verdana');   //����� - ��� ������
    End;
    result:=CreateFontIndirect(LogFont);
  end;

  Function TNR_36_NORMAL:HFont;
  var
    LogFont:TLogFont;
  begin
    FillChar(LogFont, SizeOf(LogFont), 0);
    With LogFont do
    Begin
      lfHeight                := 36;                // ������ ������
      lfWidth                 := 18;                // ������ ������
      lfEscapement            := 0;                 // ����, � ������� �������, ����� �������� ������� � ���� � ����������. ������ ����� ����������� ������� ����� ������ ������.
      lfOrientation           := 0;                 // ����, � ������� ����� �������, ����� ������� ����� ������� ��������� � ��� � ����������.
      lfWeight                := 100;          // ��� ������ � ��������� �� 0 �� 1000. ��������, 400 �������� ���������� � 700 ����������. ���� ��� �������� ����� ����, ��� ���������.
      lfItalic                := 0;                 // ��������� �����, ���� ����������� �������� TRUE .
      lfUnderline             := 0;                 // ������������ ������, ���� ����������� �������� TRUE .
      lfStrikeOut             := 0;                 // ������������ ������, ���� ����������� �������� TRUE .
      lfCharSet               := RUSSIAN_CHARSET;   // ����� ��������. ��������� �������� ��������������.
      lfOutPrecision          := OUT_DEFAULT_PRECIS;// ����� ��������. �������� ������ ����������, ��������� ������ ����� ������ ��������������� ������ �������������� ������, ������ ���������� ��������, �����, ����, � ��� ������. ��� ����� ���� ���� �� ��������� ��������.
      lfClipPrecision         := CLIP_DEFAULT_PRECIS;  // �������� ���������. �������� ��������� ����������, ��� �������� �������, ������� �������� ��� ������� ���������. ��� ����� ���� ���� ��� ��������� �� ��������� ��������.
      lfQuality               := ANTIALIASED_QUALITY;  // �������� ������.
      lfPitchAndFamily        := DEFAULT_PITCH;  // ������ � ��������� ������.
      StrCopy(lfFaceName, 'Times New Roman');   //����� - ��� ������
    End;
    result:=CreateFontIndirect(LogFont);
  end;

  Procedure CreateFonts(device: IDirect3DDevice8);
  begin
    D3DXCreateFont(device, Verdana10Bold(), DX_FONT_VERDANA_10_BOLD);
    D3DXCreateFont(device, TNR_36_NORMAL(), DX_FONT_TNR_36_NORMAL);
  end;


end.
