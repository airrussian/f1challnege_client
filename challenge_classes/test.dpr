program test;

uses
  Vcl.Forms,
  main in 'main.pas' {Form1},
  _kers in '_kers.pas',
  _dataChallenge in '_dataChallenge.pas',
  _drs in '_drs.pas',
  _energy in '_energy.pas',
  _tire in '_tire.pas',
  _car in '_car.pas',
  _wheel in '_wheel.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
