unit _kers;

interface

  uses _dataChallenge;

  type
    TKers = class(TDataChallenge)
    private
      usekers : Boolean;
      total   : Single;
      balance : Single;
      lastUse : Extended;
    public
      constructor Create(newTotal:Single=6.7);
      function use(s:boolean):boolean;
      function GetPercent:Single;
      procedure init;
    end;

implementation

{ TKers }

constructor TKers.Create(newTotal:Single=6.7);
begin
  total:=newTotal;
end;

function TKers.GetPercent: Single;
begin
  if (usekers) then
  begin
    if (balance > 0) then
    begin
      balance:=balance - (timenow - lastUse) * 10000;
      // ���������� ����
    end else
    begin
      balance:=0;
      usekers:=false;
    end;
  end;
  result := balance / total;
end;

procedure TKers.init;
begin
  balance:=total;
end;

{ ���� S ������, �� ���� ���� ���� ����� ������ ���� �� ������������� � ���������� ������, � ��������� ������ ���� }
function TKers.use(s:boolean):boolean;
begin
  if (s = false) then
  begin
    usekers:=false;
    result:=false;
  end else
  begin
    if (balance <= 0) then
    begin
      balance:=0;
      usekers:=false;
    end else
    begin
      lastUse:=timenow;
      usekers:=true;
    end;
  end;
  result:=usekers;
end;


end.
