unit _drs;

interface

	uses _dataChallenge;

	Type
		TDRSStatus = (DRS_ENABLED, DRS_DISABLED, DRS_OPEN, DRS_DETECT);
		
		TDRS = class(TDataChallenge)
		private
			status:TDRSStatus;
		public
			function open:boolean;
			procedure close;				
		end;

implementation

{ TDRS }

procedure TDRS.close;
begin
	status:=DRS_ENABLED;
end;

function TDRS.open:boolean;
begin
	if (status = DRS_DETECT) then status:=DRS_OPEN;	
end;

end.
