unit uNetFile;

interface

	uses System.SysUtils, System.Classes, Wininet, Vcl.StdCtrls;

	Type
	TDownloadThread = class(TThread)
		private			
			AppName:String;
			FileURL:String;
			FileName:String;	
			Progress: Cardinal;
		protected
			procedure Execute; override;
		public 			
			Procedure SetParam(const sAppName, sfileURL, sFileName: string);
			Function GetProgress():Cardinal;
	end;
	
implementation

	function TDownloadThread.GetProgress: Cardinal;
	begin
		result:=Progress;
	end;

	Procedure TDownloadThread.SetParam(const sAppName, sfileURL, sFileName: string);
	begin		
		AppName:=sAppName;
		FileURL:=sfileURL;
		FileName:=sFileName;
		Progress:=0;
	end;
		
	procedure TDownloadThread.Execute();
	const
		BufferSize = 1024;
	var
		hSession, hURL: HInternet;
		Buffer: array[1..BufferSize] of Byte;
		BufferLen: Cardinal;
		f: file;
		sAppName: string;
		res: pchar;
		dwBuffer:array[1..20] of Char;
		dwBufferLen,dwIndex:Cardinal;
	begin
		hSession := InternetOpen(PChar(AppName), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
		try
			hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0);
			try
						CreateDir(ExtractFilePath(FileName));			
						AssignFile(f, FileName);
						Rewrite(f,1);
						repeat
							Sleep(50);
							InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen);
							BlockWrite(f, Buffer, BufferLen);
							Progress:=Progress+BufferSize;
						until BufferLen = 0;
						CloseFile(f);
			finally
				InternetCloseHandle(hURL);
			end;
		finally
			InternetCloseHandle(hSession);
		end;
	end;
	
end.
