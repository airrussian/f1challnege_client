unit draw;

interface

  uses Direct3D8, D3DX8, System.SysUtils, Winapi.Windows, System.Classes;

  function init(device: IDirect3DDevice8):boolean;

  procedure free();

  procedure speedmetr(S:Single);

  procedure kers(sec:single);

  procedure drs(status:byte);

  procedure gear(n:shortint);

  procedure RPM(S: Double);

  procedure Text(x,y:integer; t:string);

  procedure throttle(p:single);

  procedure break(p:single);


implementation

  uses DXFont;

  var
    g_pDevice: IDirect3DDevice8 = nil;

    g_pTexture:  IDirect3DTexture8 = nil; // ��������
    g_pSprite:   ID3DXSprite       = nil; // ������

    fn_Texture:PChar = 'speed.tga';

    SpeedX:Word = 0;
    SpeedY:Word = 150;

  function init(device: IDirect3DDevice8):boolean;
  var
    h:THandle;
  begin
    if (g_PDevice <> nil) then exit;

    g_PDevice:=device;

    DXFont.CreateFonts(device);

    D3DXCreateSprite( g_pDevice, g_pSprite );

    h:=LoadLibrary('speed.dll');
    D3DXCreateTextureFromResourceEx( g_pDevice, h, 'SPEED', 0, 0, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_FILTER_NONE, D3DCOLOR_XRGB( 0, 255, 0 ), nil, nil, g_pTexture );
    FreeLibrary(h);;
  end;

  procedure free();
  begin
    g_pSprite:=nil;
    g_pTexture:=nil;
    g_PDevice:=nil;
  end;

  procedure speedmetr(S:Single);
  var
    rcTex:  TRect;
    transv: TD3DXVector2;
    fntColor:D3DColor;
  begin
    SetRect( rcTex, 0, 0, 256, 256 );       // ������� ��������
    transv.x := SpeedX;                     // ���������� �����
    transv.y := SpeedY;                     // ������� �����
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );

    SetRect( rcTex, 70+SpeedX, 80+SpeedY, 160+SpeedX, 120+SpeedY);
    if (S>80) then fntColor := D3DColor_XRGB(211,79,76) else fntColor := D3DColor_XRGB(156,207,0);
    DX_FONT_TNR_36_NORMAL.DrawTextW(PWideChar(IntToStr(Round(S / 1000 * 3600))), -1, rcTex, DT_CENTER or DT_NOCLIP, fntColor);
  end;

  procedure gear(n:shortint);
  var
    rcTex:  TRect;
    transv: TD3DXVector2;
  begin
    SetRect( rcTex, 260, (n+1) * 30, 360, (n+2) * 30);
    transv.x := 63 + SpeedX;
    transv.y := 130 + SpeedY;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );
  end;

  procedure kers(sec:single);
  var
    rcTex:  TRect;
    transv: TD3DXVector2;
  begin
    if (sec < 0) or (sec > 6.67) then sec:=0;
    SetRect( rcTex, (trunc(sec) + 1) * 18, 260, (trunc(sec) + 2) * 18, 329);
    transv.x := SpeedX + 210;
    transv.y := SpeedY + 101;

    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );

  end;

  procedure drs(status:byte);
  var
    rcTex:  TRect;
    transv: TD3DXVector2;
  begin
    SetRect( rcTex, 113, 260 + 26 * status, 150, 260 + 26 * (status + 1));
    transv.x := SpeedX + 192;
    transv.y := SpeedY + 176;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );
  end;

  procedure Text(x,y:integer; t:string);
  var
    fntColor:D3DColor;
    rcTex:  TRect;
  begin
    fntColor := D3DColor_XRGB(255,255,255);
    SetRect( rcTex, x, y, x + length(t) * 15, y + 15);
    DX_FONT_VERDANA_10_BOLD.DrawTextW(PWideChar(t), -1, rcTex, DT_LEFT or DT_NOCLIP, fntColor);
  end;

  procedure RPM(S: Double);
  const
    MAXRPM = 1800;
  var
    rcTex: TRect;
    transv: TD3DXVector2;
    rotv: TD3DXVector2;
    f:Double;
  begin
    SetRect( rcTex, 0, 394, 85, 396);
    transv.x := 113+SpeedX;
    transv.y := 131+SpeedY;
    rotv.x := 113+SpeedX; rotv.y:=131+SpeedX;

    f := - D3DX_PI/2 - S * ( (7 * D3DX_PI) / (4 * MAXRPM) );

    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, f, @transv, $FFFFFFFF );
  end;

  procedure throttle(p:single);
  var
    rcTex: TRect;
    transv: TD3DXVector2;
  begin
    if (p<0) then p:=0;
    if (p>1) then p:=1;

    SetRect( rcTex, 260, 280, 348, 296);
    transv.x := 70  + SpeedX;
    transv.y := 170 + SpeedY;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );

    SetRect( rcTex, 260, 297, round(p * 88) + 260, 313);
    transv.x := 70  + SpeedX;
    transv.y := 170 + SpeedY;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );
  end;

  procedure break(p:single);
  var
    rcTex: TRect;
    transv: TD3DXVector2;
  begin
    if (p<0) then p:=0;
    if (p>1) then p:=1;

    SetRect( rcTex, 260, 313, 348, 329);
    transv.x := 70  + SpeedX;
    transv.y := 186 + SpeedY;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );

    SetRect( rcTex, 260, 330, round(p * 88) + 260, 346);
    transv.x := 70  + SpeedX;
    transv.y := 186 + SpeedY;
    g_pSprite.Draw( g_pTexture, @rcTex, nil, nil, 0, @transv, $FFFFFFFF );
  end;


end.
