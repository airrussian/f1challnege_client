program f1client;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {Form1},
  uUpdataCenter in 'uUpdataCenter.pas',
  uDebug in 'uDebug.pas',
  uSecurityApp in 'uSecurityApp.pas',
  vgzipper in 'vgzipper.pas',
  uFiles in 'uFiles.pas',
  uNetFile in 'uNetFile.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
