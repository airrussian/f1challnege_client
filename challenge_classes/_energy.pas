unit _energy;

interface

	uses _dataChallenge;

	const
		addressFuel:Cardinal = $007C75C4;
		addressTempOid:Cardinal = $007C9174;
		addressTempWater:Cardinal = $007C91B4;
	
	type
		TEnergy = class(TDataChallenge)
		private		
			tempOil:Double;
			tempWater:Double;
			fuel:Double;
		public 
			procedure scan(); 
		end;

implementation

{ TEnergy }

procedure TEnergy.scan;
begin
	tempOil:=Self.GetDouble(addressTempOid);
	tempWater:=Self.GetDouble(addressTempWater);	
	fuel:=Self.GetDouble(addressFuel);
end;

end.
