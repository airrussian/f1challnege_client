unit uDebug;

interface

  uses System.SysUtils, System.Classes;

  // FIX ME, ����������� �� ������������� ������!

  function init(filename:string):boolean;

  procedure debug(mes:String);

  procedure save;

implementation

  var
    debugEnabled:Boolean=False;
    debugFile:TextFile;

  function init(filename:string):boolean;
  begin
    debugEnabled:=True;
    AssignFile(debugFile, filename);
    if (FileExists(filename)) then Append(debugFile) else Rewrite(debugFile);
  end;

  procedure debug(mes:String);
  var
    s:string;
  begin
    if (not debugEnabled) Then exit;
    s:='['+ DateTimeToStr(Now()) + ']: ' + mes;
    Writeln(debugFile, s);
  end;

  procedure save;
  begin
    if debugEnabled then CloseFile(debugFile);
  end;

end.
