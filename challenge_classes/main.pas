unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses _kers;

var
  Kers:TKers;
  t:Cardinal;

procedure Control;
begin
  while true do
	begin
    if GetAsyncKeyState(VK_F2) <> 0 then Kers.use(True) else Kers.use(False);
    sleep(100);
  end;
end;

procedure Calc;
begin
  while true do
  begin
    Form1.Label1.Caption:=FloatToStr(Kers.GetPercent);
    sleep(5);
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  CreateThread(nil, 0, @Control, nil, 0, t);
  CreateThread(nil, 0, @Calc, nil, 0, t);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Kers:=TKers.create;
  Kers.Init;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Kers.Destroy;
end;

end.
